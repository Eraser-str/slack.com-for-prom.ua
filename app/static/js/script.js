// возвращаем cookie с именем name
function getCookie (name) {
    var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}

function scrollToBottom (blockId) {
	$('#' + blockId).scrollTop($('#' + blockId)[0].scrollHeight);
}

function highlight (code) {
	return '<pre><code>' + hljs.highlightAuto(code).value + '</code></pre>'
}

function unixToDatetime (unixtime) {
	un = new Date(data['datetime'])
	return un.format("dd.mm.yyyy hh:MM:ss")
}