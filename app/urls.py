from http_handlers import MainHandler, IndexHandler, RegisterHandler, LoginHandler, LogoutHandler, CreateChannel

urls = [
    (r'/', MainHandler),
    (r'/user/add', RegisterHandler),
    (r'/user/login', LoginHandler),
    (r'/user/logout', LogoutHandler),
    (r'/index', IndexHandler),
    (r'/channel/add', CreateChannel),
]