# -*- coding: utf-8 -*-
import time
import logging
from cgi import escape

import tornadio2.conn

from models import db, get_link_attach
from settings import URL_PATTERN


class ChatHandler(tornadio2.conn.SocketConnection):
    participants = []

    def on_message(self, data):
        user = db.users.find_one({'auth_key': data['auth_key']})

        if not user or \
        not data['message'] or \
        not data['to_channel']:
            # shut_off(user['auth_key'])
            return

        attach_list = []
        if 'http' in data['message']:
            if URL_PATTERN.findall(data['message']):
                attach_list = [get_link_attach(link) for link in URL_PATTERN.findall(data['message'])]


        if data['to_channel'] in user['channels']:
            channel_parts = [u['auth_key'] for u in list(db.users.find({'channels': {'$in': [data['to_channel']]}},
                                                                       {'auth_key': 1, '_id': 0}))]
            for p in self.participants:
                if p['auth_key'] in channel_parts:
                    p['self_obj'].send({
                        'to_channel': data['to_channel'],
                        'avatar': user['avatar'],
                        'public_login': user['public_login'],
                        'content': escape(data['message']),
                        'datetime': int(time.time()),
                        'attach_list': attach_list
                    })
            try:
                db.channels.update(
                    {'channel_name': data['to_channel']},
                    {'$push': {
                        'messages': {
                            'avatar': user['avatar'],
                            'public_login': user['public_login'],
                            'content': data['message'],
                            'datetime': int(time.time()),
                            'attach_list': attach_list
                        }
                    }}
                )
            except:
                logging.error('on message, channels.update()')

    def on_close(self):
        # self.participants.remove(self)
        pass

    @tornadio2.event('addToOnline')
    def add_to_online(self, *args, **kwargs):
        user = db.users.find_one({'auth_key': kwargs['auth_key']})

        if not user:
            # shut_off(user['auth_key'])
            return

        self.participants.append({
            'auth_key': kwargs['auth_key'],
            'self_obj': self
        })

    @tornadio2.event('remChannel')
    def rem_channel(self, *args, **kwargs):
        user = db.users.find_one({'auth_key': kwargs['auth_key']})

        if not user or \
        not kwargs['channel_name']:
            # shut_off(user['auth_key'])
            return

        db.users.update(
            {'auth_key': kwargs['auth_key']},
            {'$pull': {
                'channels': kwargs['channel_name']
            }}
        )

        db.channels.update(
            {'channel_name': kwargs['channel_name']},
            {'$pull': {
                'participants': {'public_login': user['public_login']}
            }}
        )

        channel_parts = [u['auth_key'] for u in list(db.users.find({'channels': {'$in': [kwargs['channel_name']]}},
                                                                   {'auth_key': 1, '_id': 0}))]
        for p in self.participants:
            if p['auth_key'] in channel_parts:
                p['self_obj'].send(user['public_login'] + ' leave from channel')

    @tornadio2.event('returnHistory')
    def return_history(self, *args, **kwargs):
        user = db.users.find_one({'auth_key': kwargs['auth_key']})
        if not user:
            return

        if kwargs['channel_name'] in user['channels']:
            print 'if'
            for i in sorted(filter(lambda x: x['datetime'] > 1404763553, list(db.channels.find({'channel_name': kwargs['channel_name']}, {'messages': 1, '_id': 0}))[0]['messages']), key=lambda x: x['datetime'], reverse=True):
                print i
            try:
                return sorted(filter(lambda x: x['datetime'] > int(kwargs['last_message']), list(db.channels.find({'channel_name': kwargs['channel_name']}, {'messages': 1, '_id': 0}))[0]['messages']), key=lambda x: x['datetime'], reverse=True)

            except:
                return []

    @tornadio2.event('joinToChannel')
    def join_to_channel(self, *args, **kwargs):
        user = db.users.find_one({'auth_key': kwargs['auth_key']})
        print user['channels']
        if not user:
            return

        if db.channels.find_one({'channel_name': kwargs['name']}) and \
        kwargs['name'] not in user['channels']:
            try:
                db.channels.update(
                    {'channel_name': kwargs['name']},
                    {'$push': {
                        'participants': {'avatar': user['avatar'], 'public_login': user['public_login']}
                    }}
                )
                db.users.update(
                    {'auth_key': user['auth_key']},
                    {'$push': {
                        'channels': str(kwargs['name'])
                    }}
                )
            except:
                logging.error('joinToChannel database err')
            channel_parts = [u['auth_key'] for u in list(db.users.find({'channels': {'$in': [kwargs['name']]}},
                                                                       {'auth_key': 1, '_id': 0}))]
            for p in self.participants:
                if p['auth_key'] in channel_parts:
                    p['self_obj'].send({
                        'to_channel': kwargs['name'],
                        'avatar': user['avatar'],
                        'public_login': user['public_login'],
                        'content': user['public_login'] + ' joined',
                        'datetime': int(time.time()),
                        'attach_list': []
                    })
            db.channels.update(
                {'channel_name': kwargs['name']},
                {'$push': {
                    'messages': {
                        'avatar': user['avatar'],
                        'public_login': user['public_login'],
                        'content': user['public_login'] + ' joined',
                        'datetime': int(time.time()),
                        'attach_list': []
                    }
                }}
            )