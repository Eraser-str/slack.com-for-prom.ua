import re
import os


URL_PATTERN = re.compile(r'(https?://\S+)')


FORBIDDEN_NAMES = (
    '',
    'admin',
    'root',
    'bot'
)
DEFAULT_AVATAR = '/static/img/user_dump.jpg'

http_app_settings = {
    'debug': True,
    'static_path': os.path.join(os.path.dirname(__file__), "static"),
    'template_path': os.path.join(os.path.dirname(__file__), "templates"),
    'login_url': '/index',
}
sock_app_settings = {
    'flash_policy_port': 843,
    'flash_policy_file': 'static/flashpolicy.xml',
    'socket_io_port': 8002
}