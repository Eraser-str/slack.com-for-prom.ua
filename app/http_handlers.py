# -*- coding: utf-8 -*-
import tornado.web

from settings import FORBIDDEN_NAMES, DEFAULT_AVATAR
from models import db, hash_password, auth_key_gen


class CreateChannel(tornado.web.RequestHandler):
    @tornado.web.authenticated
    def post(self, *args, **kwargs):
        """
        :param name: channel name

        :type name: str

        :return: redirect to MainHandler

        """
        # Validation unit
        if not 1 < len(self.get_argument('name').strip()) < 36:
            self.write('''
                название канала должно иметь больше одного символа и меньше 36ти, а еще и не должно быть равным bot
            ''')
            self.redirect('/')

        if db.channels.find_one({'channel_name': self.get_argument('name').strip()}):
            self.render('chat.html', flash='Канал с таким именем уже существует')
            return

        if self.get_argument('name').strip() in FORBIDDEN_NAMES:
            self.render('chat.html', flash='Такое имя запрещено')
            return

        # Database
        try:
            channel = {
                'channel_name': self.get_argument('name').strip(),
                'participants': [{
                    'avatar': self.current_user['avatar'],
                    'public_login': self.current_user['public_login']
                }],
                'messages': []
            }

            db.channels.insert(channel)
            db.users.update(
                {'auth_key': self.current_user['auth_key']},
                {'$push': {
                    'channels': str(channel['channel_name'])
                }}
            )
        except:
            self.write('что-то пошло не так, попробуйте еще раз')
        self.redirect('/')

    def get_current_user(self):
        """return current user in current_user variable (func from tornado doc for tornado.web.authenticated decorator)

        """
        return db.users.find_one({"auth_key": self.get_cookie('auth')})


class MainHandler(tornado.web.RequestHandler):
    @tornado.web.authenticated
    def get(self):
        self.render('chat.html',
                    login=self.current_user['public_login'],
                    avatar=self.current_user['avatar'],
                    channels=self.current_user['channels'],
                    channels_name_list=[c['channel_name'].encode('utf-8') for c in
                                        db.channels.find({}, {'_id': 0, 'channel_name': 1})]
        )

    def get_current_user(self):
        """return current user in current_user variable (func from tornado doc for tornado.web.authenticated decorator)

        """
        return db.users.find_one({"auth_key": self.get_cookie('auth')})


class IndexHandler(tornado.web.RequestHandler):
    def get(self, *args, **kwargs):
        self.render('index.html')


class RegisterHandler(tornado.web.RequestHandler):
    def post(self, *args, **kwargs):
        """
        :param public_login:
        :param password:
        :param private_login:

        """
        public_login = self.get_argument('public_login').lower().strip()
        password = self.get_argument('password').lower().strip()
        private_login = self.get_argument('private_login').lower().strip()

        # validation unit
        if len(public_login) > 36 or \
        len(password) > 255 or \
        len(private_login) > 255:
            return self.render('index.html', flash=['data is invalid'])

        if public_login in FORBIDDEN_NAMES or \
        db.users.find_one({"public_login": public_login}) is not None:
            return self.render('index.html', flash=['user already exist'])

        # registration
        password = hash_password(password)
        auth_key = auth_key_gen(password)

        db.users.insert({
            "public_login": public_login,
            "private_login": private_login,
            "password": password,
            "avatar": DEFAULT_AVATAR,
            "auth_key": auth_key,
            "channels": []
        })

        # login
        self.set_cookie('auth', auth_key)
        self.redirect('/')


class LoginHandler(tornado.web.RequestHandler):
    def post(self, *args, **kwargs):
        """
        :param private_login:
        :param password:

        """
        user = db.users.find_one(
            {
                "private_login": self.get_argument('private_login'),
                "password": hash_password(self.get_argument('password'))
            }
        )
        if not user:
            return self.render('index.html', flash=['user not found'])

        auth_key = auth_key_gen(user['password'])
        # update user auth key in database
        db.users.update(user, {"$set": {'auth_key': auth_key}})

        # login
        self.set_cookie('auth', auth_key)
        self.redirect('/')


class LogoutHandler(tornado.web.RequestHandler):
    def get(self, *args, **kwargs):
        self.clear_cookie('auth')
        self.redirect('/')