# -*- coding: utf-8 -*-
import hashlib
import random
from string import ascii_letters, digits

import pymongo
import redis

from parser import Parser


def db_connect():
    conn = pymongo.Connection()
    db = conn['Slack']
    return db

db = db_connect()
r = redis.Redis()



def get_next_sequence(name):
    """http://docs.mongodb.org/manual/tutorial/create-an-auto-incrementing-field/

    """
    db.counters.update({"_id": name}, {'$inc': {"seq": 1}})
    return db.counters.find_one({"_id": name})['seq']


def hash_password(passwd):
    password = hashlib.md5(passwd).hexdigest()
    for i in range(16):
        password = hashlib.sha1(password).hexdigest()
    return password


def str_gen(length):
    """Generates a random string

    :param length: string length

    :rtype: str

    """
    return "".join(random.choice(ascii_letters + digits) for _ in range(length))


# TODO: коллизии
def auth_key_gen(base_str):
    """Generates a random string based on a base string (typically a user password) to be used as an authorization key.
    It is necessary to under one account could not sit by different people with different computers, and we do not show
    even the password in a cookie.

    :param base_str: string becomes the basis for the authorization key

    :type base_str: str

    :return: sha1 hash in hexadecimal representation
    :rtype: str

    """
    string = base_str + str_gen(32)
    auth_key = [s for s in string]
    random.shuffle(auth_key)
    auth_key = "".join(i for i in auth_key)
    return hashlib.sha1(auth_key).hexdigest()


def get_link_attach(url):
    """Adds a link and its data into the cache, if not already there, or take the data from the cache links. and then
    returns the data links.

    :rtype: dict
    """
    site = Parser(url)
    if not r.get(url):
        r.set(url,
            {
                'title': site.get_title(),
                'desc': site.get_description(),
                'url': url
            }
        )
        return {'title': site.get_title(), 'desc': site.get_description(), 'url': url}
    else:
        return eval(r.get(url))