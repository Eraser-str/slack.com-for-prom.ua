from grab import Grab


class Parser:
    def __init__(self, url):
        self.g = Grab()
        self.g.go(url)

    def get_title(self):
        try:
            return self.g.tree.xpath('//title/text()')[0]
        except IndexError:
            return None

    def get_description(self):
        try:
            return self.g.tree.xpath('//meta[@name="description"]/@content')[0]
        except IndexError:
            return None


# a = Parser('http://habrahabr.ru/post/228831/')
# print a.get_description()

