import datetime
import logging

import tornado.ioloop
import tornado.web
import tornado.httpserver
import tornadio2.router
import tornadio2.server

from urls import urls
from settings import http_app_settings, sock_app_settings
from sock_handlers import ChatHandler


http_app = tornado.web.Application(
    urls,
    **http_app_settings
)

sock_router = tornadio2.router.TornadioRouter(ChatHandler)
sock_app = tornado.web.Application(
    sock_router.urls,
    **sock_app_settings
)

def main():
    logging.basicConfig(level=logging.INFO)
    logging.info(str(datetime.datetime.now()) + " | server starting")

    http_server = tornado.httpserver.HTTPServer(http_app)
    http_app.listen(8001)

    tornadio2.server.SocketServer(sock_app)

    tornado.ioloop.IOLoop.instance().start()

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    logging.info(str(datetime.datetime.now()) + " | server starting")

    http_server = tornado.httpserver.HTTPServer(http_app)
    http_app.listen(8001)

    tornadio2.server.SocketServer(sock_app)

    tornado.ioloop.IOLoop.instance().start()
