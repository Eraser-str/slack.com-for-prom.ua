## Базовый функционал

1. Регистрация пользователя
2. Создание чат канала
3. Поиск чат каналов
4. Присоединение к чат каналу
5. Написание нового сообщения
6. Выход из чат канала

## Дополнительный функционал

* Бот выполняющий команды
* Загрузка превью ссылок

## Технологии/фреймворки/библиотеки

* Клиент
    * Jquery
    * socket.io
* Сервер
    * Tornado + TornadIO2
    * grab
    * ...
* Базы данных
    * Mongodb
    * Redis

## Демонстрация

![title](https://bitbucket.org/Eraser-str/slack.com-for-prom.ua/raw/41f1866174878848bc40856a4a2aea27b472ef9c/screens/mainpage.png?at=master)

![title](https://bitbucket.org/Eraser-str/slack.com-for-prom.ua/raw/41f1866174878848bc40856a4a2aea27b472ef9c/screens/afterreg.png?at=master)

![title](https://bitbucket.org/Eraser-str/slack.com-for-prom.ua/raw/41f1866174878848bc40856a4a2aea27b472ef9c/screens/createchannel.png?at=master)

![title](https://bitbucket.org/Eraser-str/slack.com-for-prom.ua/raw/41f1866174878848bc40856a4a2aea27b472ef9c/screens/joinuser.png?at=master)

![title](https://bitbucket.org/Eraser-str/slack.com-for-prom.ua/raw/41f1866174878848bc40856a4a2aea27b472ef9c/screens/newcounter.png?at=master)

![title](https://bitbucket.org/Eraser-str/slack.com-for-prom.ua/raw/41f1866174878848bc40856a4a2aea27b472ef9c/screens/newmessages.png?at=master)

![title](https://bitbucket.org/Eraser-str/slack.com-for-prom.ua/raw/41f1866174878848bc40856a4a2aea27b472ef9c/screens/boteval.png?at=master)

![title](https://bitbucket.org/Eraser-str/slack.com-for-prom.ua/raw/41f1866174878848bc40856a4a2aea27b472ef9c/screens/botcommand.png?at=master)

![title](https://bitbucket.org/Eraser-str/slack.com-for-prom.ua/raw/41f1866174878848bc40856a4a2aea27b472ef9c/screens/sqbot.png?at=master)

![title](https://bitbucket.org/Eraser-str/slack.com-for-prom.ua/raw/41f1866174878848bc40856a4a2aea27b472ef9c/screens/linkattach.png?at=master)